<!DOCTYPE html>

<html>
  <head>
    <!-- En-tête de la page -->
    <meta charset="utf-8" />
    <title>Stegit</Title>
    <link rel="stylesheet" href="assets/bootstrap.min.css"></link>
    <link rel="stylesheet" href="assets/bootstrap.min.css.map"></link>
    <link rel="stylesheet" href="assets/image-picker.css"></link>
    <script src="assets/jquery-3.3.1.min.js"></script>
    <script src="assets/bootstrap.min.js"></script>
    <script src="assets/bootstrap.min.js.map"></script>
    <script src="assets/image-picker.min.js"></script>
    <style>
      body {
        padding-top: 54px;
      }
      ul.thumbnails.image_picker_selector li {
        box-sizing: content-box !important;
      }
      .image_picker_image {
        max-width: 250px;
      }
      @media (min-width: 992px) {
        body {
          padding-top: 56px;
        }
      }
    </style>
  </head>
  <body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">5dePik - LocalStegHide <img src="assets/images/spades.png" alt="" style="max-height: 20px;"/></a>
        <div class="" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <!-- formulaire d'ajout d'image -->
            <form id="image-upload-form" method="post" action="upload.php" enctype="multipart/form-data">
              <li class="nav-item active">
                <label style="margin:0" class="btn btn-dark">
                  Upload image <input id="image-uploader" name="fileToUpload" type="file" hidden>
                </label>
                <!--<a class="nav-link" href="#"><button type="submit" class="btn btn-dark">Upload image</button></a>-->
              </li>
            </form>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->
    <section class="py-3">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h1 class="mt-5">Select image to cover the message</h1>
            <p class="lead">Don't tell a secret to anybody, unless you want the whole world to know it.</p>
            <form method="post" action="stegit.php">
              <label id="file"></label><br>
              <div class="table">
                <select id="images" name="file">
                  <?php include 'images.php';?>
                </select>
              </div>
              <div class="form-group row">
                <div class="col-3 offset-2">
                  <p class="lead">Message to encode</p>
                </div>
                <div class="col-5">
                  <textarea class="form-control" name="message" rows="10" cols="50" placeholder="Type here your secret message..." required=required></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-3 offset-2">
                  <p class="lead">Passphrase to use</p>
                </div>
                <div class="col-4">
                  <input class="form-control" type="text" name="passphrase" rows="10" placeholder="Type your secret passphrase..." cols="50"></input>
              </div>
              <div class="col-1">
                <button type="submit" class="btn btn-dark">Stegit !</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <!-- Footer -->
  <footer class="py-2 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; 5dePik 2018</p>
    </div>
    <!-- /.container -->
  </footer>
  <script>
    $("select").imagepicker();
  </script>
  <script>
    document.getElementById("image-uploader").onchange = function() {
      document.getElementById("image-upload-form").submit();
      window.console.log("submitted?");
    };
  </script>
</body>
  </html>
