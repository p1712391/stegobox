\documentclass[notitlepage,11pt]{article}
\usepackage{geometry}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsthm}
\usepackage{relsize}
\usepackage{hyperref}
\usepackage[french]{babel}
\usepackage{titling}
\usepackage{xcolor}
\usepackage{listings}

\usepackage{enumitem}
\renewcommand\descriptionlabel[1]{#1 :}

\setlength{\parindent}{0.5cm}
\setlength{\parskip}{0.1em}
% minted for code highlightning, use xelatex to compile
% eg. pdflatex -synctex=1 -interaction=nonstopmode --shell-escape file.tex
\usepackage[cache=false]{minted}
\hypersetup{colorlinks, linkcolor={red!50!black}, citecolor={blue!50!black}, urlcolor={blue!80!black}}
\begin{document}
\author{Rafael RAMOS TUBINO p1308146,\\ Robert KALNA p1712391,\\ Thomas BONIS p1409417}
\title{Tutoriel de StegoBox}
\date{\today}
\maketitle
\thispagestyle{empty}

\frenchspacing

\section{Introduction}

Ce tutoriel explique comment créer une clé USB avec un serveur web contenant une page capable de cacher un message dans une image.
Le sujet nous a été proposé lors des cours de Programmation Embarqué, à l'Université Lyon 1, Printemps 2018.


\section{Préparer d'une clé bootable avec un système d'exploitation basique}

Ici nous reprenons simplement les étapes du TP vu en cours :
\begin{description}[style=nextline]
    \item[Passer root] \mintinline{shell}{sudo -i} ou \mintinline{shell}{su}
    \item[On suppose que la clé sera sous le nom] \mintinline{shell}{sdX}
    \item[Créer ou s'assurer d'avoir une partition primaire (1Go sont suffisant)] 
    \mintinline{shell}{fdisk /dev/sdX}
    \item[Formater cette partition] \mintinline{shell}{mkfs.ext4 /dev/sdX1}
    \item[Créer un point de montage] \mintinline{shell}{mkdir mountpoint}
    \item[Monter la partition] \mintinline{shell}{mount /dev/sdX1 mountpoint}
    \item[Installer debootstrap] \mintinline{shell}{apt install debootstrap}
    \item[Lancer debootstrap] \mintinline{shell}{debootstrap --arch amd64 jessie mountpoint http://ftp.fr.debian.org/debian}
    \item[Liez les dossiers proc et dev du poste à ceux de la clé USB] \mintinline{shell}{mount -t proc none mountpoint/proc} 
    \\\mintinline{shell}{mount -o bind /dev mountpoint/dev}
    \item[Chrooter] \mintinline{shell}{chroot mountpoint}
    \item[Initialiser un mot de passe] \mintinline{shell}{passwd}
    \item[Installer un noyau] \mintinline{shell}{apt-get update ; apt-get install linux-image-amd64}
    \item[Récupérer l'UUID de la clé USB] \mintinline{shell}{ls -l /dev/disk/by-uuid/}
    \item[Éditer le fichier fstab] \mintinline{shell}{vim.tiny.tiny /etc/fstab} \begin{lstlisting} 
proc /proc proc defaults
UUID=xxxxxxxxxxx    /    ext4 errors=remount-ro 0 1
    \end{lstlisting}
    \item[Initialiser le hostname] \mintinline{shell}{echo "5pik" > /etc/hostname}
    \item[Editer le fichier /etc/network/interfaces] \begin{lstlisting} 
auto lo
iface lo inet loopback

allow-hotplug eth0
auto eth0
iface eth0 inet dhcp
    \end{lstlisting}
    \item[(Optionel) Support pour clavier azerty] \mintinline{shell}{apt install console-data}
    \item[Installer grub2] \mintinline{shell}{apt install grub2}
    \item[Quitter le chroot] \mintinline{shell}{exit}
\end{description}

\section{Installation de l'application web}
\paragraph{}
L'application permet d'envoyer des images sur le serveur. Ces images peuvent être sélectionnées pour y encoder un message, à l'aide d'un mot de passe.

Une fois le formulaire remplis, les informations sont envoyées en POST au script php suivant :
\begin{minted}[breaklines]{php}
<?php
    header('Content-Type: application/download');
    header('Content-Disposition: attachment; filename='.$_POST['file']);
    header("Content-Length: " . filesize($_POST['file']));

    exec("./script.sh ".$_POST['file']." \"".$_POST['message']."\"\"".$_POST['passphrase']."\"");

    $fp = fopen($_POST['file'], "r");
    fpassthru($fp);
    fclose($fp);

    exec("./deleteTemp.sh ".$_POST['file']);
    echo '<script>window.location.href = "/";</script>';

?>
\end{minted}

Ce script appelle par la suite le script shell \emph{script.sh} qui utilise l'outil \emph{steghide} pour encoder le message dans l'image :

\begin{minted}{shell}
echo "$2" > message
cp images/$1 ./working_image
steghide embed -cf working_image -ef message -p $3
mv working_image $1
\end{minted}

L'image résultante est alors envoyée à l'utilisateur, puis le script \emph{deleteTemp.sh} est utilisé pour nettoyer le serveur des fichiers temporaires générés :

\begin{minted}{shell}
rm message
rm working_image
rm $1
\end{minted}

Pour faire fonctionner l'application, nous proposons de récupérer la page web que nous avons créée et d'installer convenablement le serveur sur la clé USB. Pour ce faire :
\begin{description}[style=nextline]
  \item[Récupérer la page web et le script pour installer le serveur] \mintinline{shell}{git clone https://forge.univ-lyon1.fr/p1712391/stegobox}
    \item[Copier les répertoires au root de notre système] \mintinline{shell}{cp -r stegobox/* mountpoint/root/}
    \item[Passer en chroot de nouveau] \mintinline{shell}{chroot mountpoint}
    \item[Naviguer dans le répertoire root] \mintinline{shell}{cd /root}
    \item[Lancer le script pour installer le web serveur, steghide, et figlet] 
\begin{lstlisting}
bash /root/scripts/set_up_web_server.sh
\end{lstlisting}
ou
\begin{lstlisting}
apt install -y lighttpd

# debian doesn't handle always this dependency for lighttpd
apt install -y gamin

lighty-enable-mod cgi

apt-get install -y php5-cgi

apt-get install -y steghide

lighty-enable-mod fastcgi 
lighty-enable-mod fastcgi-php

#Reload the lighttpd daemon
service lighttpd force-reload

apt-get install -y figlet
\end{lstlisting}
    \item[Copier l'application \texttt{php} dans le bon répertoire] \mintinline{shell}{cp -r /root/website/* /var/www/html/}
    \item[Trouver l'utilisateur gérant \texttt{lighttpd} (par défaut 
      \texttt{www-data} ou \texttt{lighttpd})] \mintinline{shell}{grep username /etc/lighttpd/lighttpd.conf}
    \item[Rendre cet utilisateur propriétaire du répertoire de la page web /var/www/html] \mintinline{shell}{chown -R www-data:www-data /var/www/html}
\end{description}

En supposant que le nom de l'utilisateur et du groupe sont : \texttt{www-data}.

\section{Boot direct avec Grub}
Sur la clé, désactiver le menu et les timeout de grub affin de booter directement :
\begin{description}[style=nextline]
    \item[Désactiver le menu de Grub] \mintinline{shell}{vim.tiny /etc/default/grub}
    \item[Mettre les \texttt{TIMEOUT} à 0.] \mintinline{shell}{GRUB_HIDDEN_TIMEOUT=0}
    \\\mintinline{shell}{GRUB_TIMEOUT=0}
    \item[Valider ces changements avec Grub] \mintinline{shell}{update-grub}
\end{description}

\section{Affichage de l'adresse IP}

Pour l'instant l'utilisateur arrive sur une page de login une fois le système prêt. Nous allons faire en sorte que cet écran n'apparaisse pas. Nous allons afficher l'adresse IP de notre serveur à la fin des messages du système.

Pour cela :
\begin{description}[style=nextline]
    \item[Désactiver le service de premier terminal \texttt{tty1}] \mintinline{shell}{systemctl disable getty@tty1.service}
    \item[Créez un service qui va se lancer après le service \texttt{lighttpd}, créez le fichier pour le service] 
    \mintinline{shell}{vim.tiny /etc/systemd/system/show_ip.service}
    \item[Configurer le service]
Le service doit se lancer après le service \texttt{lighttpd}
    \begin{lstlisting}
[Unit]
Description = show ip adress on boot
After = network.target lighttpd.service systemd-update-utmp.service
[Service]
ExecStart = /root/scripts/findip.sh
StandardOutput = journal+console
    \end{lstlisting}
\end{description}
Ici, \texttt{systemd-update-utmp.service} est aussi utilisé car c'est le dernier service à se lancer pendant le boot.
Le script \emph{findip.sh} est dans notre implémenté de la manière suivante : 
\begin{minted}[breaklines]{shell}
#!/bin/bash

COMM="$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')"

MYIP=${COMM}

echo "L'addresse IP locale est : $MYIP" | figlet
\end{minted}

\section{Test}
Pour tester l'application en utilisant Qemu :
\begin{description}[style=nextline]
    \item[Demontez les deux dossiers] \mintinline{shell}{umount mountpoint/{dev,proc} && umount mountpoint}
    \item[Tester] \mintinline{shell}{qemu-system-x86_64 /dev/sdX}
    \item[Changer terminal pour pouvoir se connecter] 
    \begin{lstlisting}
CTRL + ALT + F2
    \end{lstlisting}
    \item[Se connecter en tant que root et stopper proprement] 
\mintinline{shell}{shutdown -h now}
\end{description}

\section{Conclusion}
\paragraph{}
Ce projet a été testé sur QEMU avec un bon fonctionnement. Le seul problème trouvé c'est l'envoie de nouvelles images.
Lors d'un test sur un réseau physique, l'envoie de nouvelles images fonctionne, cependant la réception de l'image contenant le message ne se produit pas de la façon souhaitée.
\paragraph{}
Les problèmes de réseau (récupérer l'adresse IP, la numérotation des interfaces) ont été prédominant et ils sont sûrement la cause de nos bugs non résolus.
\paragraph{}
La quantité de connaissance requises, ainsi que le temps passé pour avoir un résultat quasiment fonctionnel, étaient surprenant. Ce genre de projet demande un temps de travail supérieur à ce à quoi on peut s'attendre pour du développement d'application "classique".

\end{document}
