#!/bin/bash

apt install -y lighttpd

apt install -y gamin

lighty-enable-mod cgi

#Enable the fastcgi module and the php configuration with
#apt-get install php7.0-cgi
apt-get install -y php5-cgi

apt-get install -y steghide

lighty-enable-mod fastcgi 
lighty-enable-mod fastcgi-php

#Reload the lighttpd daemon
service lighttpd force-reload

#cp 10-cgi.conf /etc/lighttpd/conf-available/10-cgi.conf

#/etc/init.d/lighttpd reload

#cp example.sh /usr/lib/cgi-bin/example.sh





