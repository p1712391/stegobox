#!/bin/bash

COMM="$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')"

MYIP=${COMM}

echo "L'addresse IP locale est : $MYIP" | figlet
